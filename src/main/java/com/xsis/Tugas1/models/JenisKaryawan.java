package com.xsis.Tugas1.models;

import javax.persistence.*;

@Entity
@Table(name = "jenis_karyawan")
public class JenisKaryawan extends Common {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Long id;


    @Column(nullable = false, name = "nama", insertable = false, updatable = false)
    private String nama;

    @Column(nullable = false, name = "keterangan")
    private String keterangan;

//    @ManyToOne
//    @JoinColumn(name = "nama", insertable = false, updatable = false)
//    public Karyawan nama;
//
//    @Column(name = "nama_karyawan", nullable = false)
//    private String namaKaryawan;

    public Long getId() {
        return id;
    }

//    public String getNama() {
//        return namaJenis;
//    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

//    public void setNama(String namaJenis) {
//        this.namaJenis = namaJenis;
//    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

//    public Karyawan getNama() {
//        return nama;
//    }
//
//    public void setNama(Karyawan nama) {
//        this.nama = nama;
//    }
//
//    public String getNamaKaryawan() {
//        return namaKaryawan;
//    }
//
//    public void setNamaKaryawan(String namaKaryawan) {
//        this.namaKaryawan = namaKaryawan;
//    }
}
