package com.xsis.Tugas1.models;

import javax.persistence.*;

@Entity
@Table(name ="jenis_cuti")
public class JenisCuti extends Common {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Column(nullable = true, name = "keterangan")
    private String keterangan;

    public Long getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
