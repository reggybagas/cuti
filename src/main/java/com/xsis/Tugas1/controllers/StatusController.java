package com.xsis.Tugas1.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/status/")
public class StatusController {
    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("status/index");
        return view;
    }
}
