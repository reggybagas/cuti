package com.xsis.Tugas1.controllers;

import com.xsis.Tugas1.models.JenisCuti;
import com.xsis.Tugas1.models.JenisKaryawan;
import com.xsis.Tugas1.models.Karyawan;
import com.xsis.Tugas1.repositories.JenisCutiRepo;
import com.xsis.Tugas1.repositories.JenisKaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiJenisKaryawanController {

    @Autowired
    private JenisKaryawanRepo jenisKaryawanRepo;
    @GetMapping(value="/jeniskaryawan")
    public ResponseEntity<List<JenisKaryawan>> GetAlljenisKaryawan() {
        try {
            List<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findAll();
            return new ResponseEntity<> (jenisKaryawan, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/jeniskaryawan/{id}")
    public ResponseEntity<List<JenisKaryawan>> GetJenisKaryawanById(@PathVariable("id") Long id)
    {
        try {
            Optional<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findById(id);
            if (jenisKaryawan.isPresent()) {
                ResponseEntity rest = new ResponseEntity(jenisKaryawan, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/jenisKaryawan")
    public ResponseEntity<Object> SaveJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan) {
        try {
            jenisKaryawan.setCreatedBy("Reggy");
            jenisKaryawan.setCreatedOn(new Date());
            this.jenisKaryawanRepo.save(jenisKaryawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/jeniskaryawan/{id}")
    public ResponseEntity<Object> UpdateJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan, @PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = this.jenisKaryawanRepo.findById(id);
            if (jenisKaryawanData.isPresent()) {
                jenisKaryawan.setId(id);
                jenisKaryawan.setModifieddBy("Reggy");
                jenisKaryawan.setModifiedOn(new Date());
                this.jenisKaryawanRepo.save(jenisKaryawan);
                ResponseEntity rest = new ResponseEntity<>("succes", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();

            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
    @DeleteMapping(value = "/jeniskaryawan/{id}")
    public ResponseEntity<Object> DeleteJenisKaryawan (@RequestBody JenisKaryawan jenisKaryawan, @PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = this.jenisKaryawanRepo.findById(id);
            if (jenisKaryawanData.isPresent()) {
                jenisKaryawan.setId(id);
                jenisKaryawan.setModifieddBy("Reggy");
                jenisKaryawan.setModifiedOn(new Date());
                jenisKaryawan.setDelete(true);
                this.jenisKaryawanRepo.save(jenisKaryawan);
                ResponseEntity rest = new ResponseEntity<>("succes", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

}
