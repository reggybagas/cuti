package com.xsis.Tugas1.controllers;

import com.xsis.Tugas1.models.JenisCuti;
import com.xsis.Tugas1.repositories.JenisCutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiJenisCutiController {
    @Autowired
    private JenisCutiRepo jenisCutiRepo;
    @GetMapping(value="/jeniscuti")
    public ResponseEntity<List<JenisCuti>> GetAlljenisCuti() {
        try {
            List<JenisCuti> jenisCuti = this.jenisCutiRepo.findAll();
            return new ResponseEntity<> (jenisCuti, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/jeniscuti/{id}")
    public ResponseEntity<List<JenisCuti>> GetJenisCutiById(@PathVariable("id") Long id)
    {
        try {
            Optional<JenisCuti> jenisCuti = this.jenisCutiRepo.findById(id);
            if (jenisCuti.isPresent()) {
                ResponseEntity rest = new ResponseEntity(jenisCuti, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/jeniscuti")

    //response entity untuk return message success atau enggak nya
    public ResponseEntity<Object> saveJenisCuti(@RequestBody JenisCuti jenisCuti) {
        try {
            jenisCuti.setCreatedBy("Reggy");
            jenisCuti.setCreatedOn(new Date());
            this.jenisCutiRepo.save(jenisCuti);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}

