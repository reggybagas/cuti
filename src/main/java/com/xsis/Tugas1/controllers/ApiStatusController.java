package com.xsis.Tugas1.controllers;

import com.xsis.Tugas1.models.Karyawan;
import com.xsis.Tugas1.models.Status;
import com.xsis.Tugas1.repositories.KaryawanRepo;
import com.xsis.Tugas1.repositories.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiStatusController {
    @Autowired
    private StatusRepo statusRepo;
    @GetMapping(value="/status")
    public ResponseEntity<List<Status>> GetAllStatus() {
        try {
            List<Status> status = this.statusRepo.findAll();
            return new ResponseEntity<> (status, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/status/{id}")
    public ResponseEntity<List<Status>> GetStatusById(@PathVariable("id") Long id)
    {
        try {
            Optional<Status> status = this.statusRepo.findById(id);
            if (status.isPresent()) {
                ResponseEntity rest = new ResponseEntity(status, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/status")
    public ResponseEntity<Object> saveStatus(@RequestBody Status status) {
        try {
            status.setCreatedBy("Reggy");
            status.setCreatedOn(new Date());
            this.statusRepo.save(status);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

}
