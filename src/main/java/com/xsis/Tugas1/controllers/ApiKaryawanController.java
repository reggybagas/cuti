package com.xsis.Tugas1.controllers;

import com.xsis.Tugas1.models.JenisKaryawan;
import com.xsis.Tugas1.models.Karyawan;
import com.xsis.Tugas1.repositories.JenisKaryawanRepo;
import com.xsis.Tugas1.repositories.KaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiKaryawanController {
    @Autowired
    private KaryawanRepo karyawanRepo;
    @GetMapping(value="/karyawan")
    public ResponseEntity<List<Karyawan>> GetAllKaryawan() {
        try {
            List<Karyawan> karyawan = this.karyawanRepo.findAll();
            return new ResponseEntity<> (karyawan, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/karyawan/{id}")
    public ResponseEntity<List<Karyawan>> GetKaryawanById(@PathVariable("id") Long id)
    {
        try {
            Optional<Karyawan> karyawan = this.karyawanRepo.findById(id);
            if (karyawan.isPresent()) {
                ResponseEntity rest = new ResponseEntity(karyawan, HttpStatus.OK);
                return rest;
            }
//            } else if (karyawan.get().isDelete()){
//                ResponseEntity rest = new ResponseEntity(karyawan,)
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/karyawan")
    public ResponseEntity<Object> saveKaryawan(@RequestBody Karyawan karyawan) {
        try {
            karyawan.setCreatedBy("Reggy");
            karyawan.setCreatedOn(new Date());
            this.karyawanRepo.save(karyawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/karyawan/{id}")
    public ResponseEntity<Object> UpdateKaryawan(@RequestBody Karyawan karyawan, @PathVariable("id") Long id) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);
            if (karyawanData.isPresent()) {
                karyawan.setId(id);
                karyawan.setModifieddBy("Reggy");
                karyawan.setModifiedOn(new Date());
                this.karyawanRepo.save(karyawan);
                ResponseEntity rest = new ResponseEntity<>("succes", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();

            }
        } catch (Exception ex) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
        @DeleteMapping(value = "/karyawan/{id}")
        public ResponseEntity<Object> DeleteKaryawan (@RequestBody Karyawan karyawan, @PathVariable("id") Long id)
        {
            try {
                Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);
                if (karyawanData.isPresent()) {
                    karyawan.setId(id);
                    karyawan.setModifieddBy("Reggy");
                    karyawan.setModifiedOn(new Date());
                    karyawan.setDelete(true);
                    this.karyawanRepo.save(karyawan);
                    ResponseEntity rest = new ResponseEntity<> ("succes", HttpStatus.OK);
                    return rest;
                } else {
                    return ResponseEntity.notFound().build();
                }
            } catch (Exception ex) {
                return  new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
            }
        }

    }

