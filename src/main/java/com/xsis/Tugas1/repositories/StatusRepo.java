package com.xsis.Tugas1.repositories;

import com.xsis.Tugas1.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepo extends JpaRepository<Status, Long> {
}
