package com.xsis.Tugas1.repositories;

import com.xsis.Tugas1.models.JenisCuti;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JenisCutiRepo extends JpaRepository<JenisCuti, Long> {
}
